import axios from 'axios';
import CONST from '../common/Const'; 


export default class API {
  static getUrl=(path)=>{
    return CONST.APIHOST + path
  }

  static get=(path)=>{
    return axios.get(
      API.getUrl(path),
      {
        headers: {
          'identification':localStorage.getItem('identification'),
          'Content-Type': 'application/json'
        }
      }
    ).then((response) => {
      return response
    }).catch((error) => {
      return error.response
    })
  }

  static post=(path, draft, contentType)=>{ 
    return axios.post(
      API.getUrl(path),
      draft,
      {
        headers: {
          'identification':localStorage.getItem('identification'),
          'Content-Type': contentType // 画像データを送る場合もあるためjsonで固定しない
        }
      }
    ).then((response) => {
      return response
    }).catch((error) => {
      return error.response
    })
  }

  static patch=(path, draft, contentType)=>{
    return axios.patch(
      API.getUrl(path),
      draft,
      {
        headers: {
          'identification':localStorage.getItem('identification'),
          'Content-Type': contentType // 画像データを送る場合もあるためjsonで固定しない
        }
      }
    ).then((response) => {
      return response
    }).catch((error) => {
      return error.response
    })
  }

  static destroy=(path)=>{
    return axios.delete(
      API.getUrl(path),
      {
        headers: {
          'identification':localStorage.getItem('identification')
        }
      }
    ).then((response)=>{
      return response
    }).catch((error)=>{
      return error.response
    })
  }
}
