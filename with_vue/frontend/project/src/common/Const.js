const DEV = {
    mode: 'dev',
    APIHOST: 'http://localhost:8000'
  };
      
  const PROD = {
    mode: 'prod', 
    APIHOST: window.location.origin,
  };
    
  const CONST = process.env.NODE_ENV === 'production' ? PROD: DEV;
  
  export default CONST;
  