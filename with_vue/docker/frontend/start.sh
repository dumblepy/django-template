#!/bin/sh
cd /home/node/frontend/project

yarn install

if [ "${DJANGO_ENV}" = 'production' ]; then
    yarn build
else
    yarn serve --port 3000
fi
