#!/bin/bash

# dockerの環境変数(.env)ファイル
# https://docs.docker.com/compose/environment-variables/#the-env-file
# http://docs.docker.jp/compose/env-file.html

poetry install

if [ "${DJANGO_ENV}" = 'production' ]; then
    poetry run python manage.py migrate
    poetry run python manage.py collectstatic --noinput
    poetry run gunicorn config.wsgi:application -c ../docker/etc/gunicorn.conf
else
    poetry run python manage.py migrate --settings config.settings.test
    poetry run python manage.py runserver 0.0.0.0:8000 --settings config.settings.test
fi
