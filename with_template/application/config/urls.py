from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from app.urls import app_urls

urlpatterns = [
    path("admin/", admin.site.urls),
    # path("", TemplateView.as_view(template_name="index.html")),
    path("", include(app_urls))
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
