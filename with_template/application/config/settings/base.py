import os
import sys

import environ

###############
# Build paths #
###############
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
APP_ROOT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
sys.path.append(APP_ROOT_PATH)

# 環境変数の読み込み
env = environ.Env()

try:
    DJANGO_ENV = env("DJANGO_ENV")
except Exception:
    DJANGO_ENV = "local"


if DJANGO_ENV == "local":
    # application/config/.envを読み込み
    READ_ENV_FILE = env.bool("DJANGO_READ_ENV_FILE", default=False)
    env_file = os.path.join(BASE_DIR, ".env")
    env.read_env(env_file)

############
# Security #
############

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/
SECRET_KEY = env("SECRET_KEY")

# SECURITY WARNING: don't run with debug turned on in production!
if DJANGO_ENV == "production":
    DEBUG = False
else:
    DEBUG = True

if DJANGO_ENV == "local" or DJANGO_ENV == "dev":
    ALLOWED_HOSTS = ["*"]
else:
    ALLOWED_HOSTS = ["nginx-internal-sock"] + env("ALLOWED_HOSTS").split(",")


##########################
# Application definition #
##########################
INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # 3rd party
    "debug_toolbar",
    "django_extensions",
    "rest_framework",
    # My Application
    "app",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "config.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [os.path.join(APP_ROOT_PATH, "templates")],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "config.wsgi.application"


#############
# DATABASES #
#############
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

if DJANGO_ENV == "production" or DJANGO_ENV == "dev":
    # used PostgreSQL
    DARABASE_CONF = {
        "driver": "postgres",
        "database": env("DB_NAME"),
        "user": env("DB_USER"),
        "password": env("DB_PASSWORD"),
        "host": "db",
        "port": 5432,
    }

    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.postgresql_psycopg2",
            "NAME": DARABASE_CONF["database"],
            "USER": DARABASE_CONF["user"],
            "PASSWORD": DARABASE_CONF["password"],
            "HOST": DARABASE_CONF["host"],
            "PORT": DARABASE_CONF["port"],
        }
    }
else:
    # used Sqlite
    DATABASES = {
        "default": {
            "ENGINE": "django.db.backends.sqlite3",
            "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
        }
    }

# セッションドライバーををRedisにする
# 使う時はRedisのDockerコンテナを追加する
# CACHES = {
#     "default": {
#         "BACKEND": "django_redis.cache.RedisCache",
#         "LOCATION": "redis://redis:6379/1",
#         "OPTIONS": {
#             "CLIENT_CLASS": "django_redis.client.DefaultClient",
#             "PASSWORD": env("REDIS_PASSWORD")
#         }
#     }
# }

#######################
# Password validation #
#######################

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]


########################
# Internationalization #
########################
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = "ja"

# TIME_ZONE = "Asia/Tokyo"
try:
    TIME_ZONE = env("TZ")
except Exception:
    TIME_ZONE = "Asia/Tokyo"

USE_I18N = True

USE_L10N = True

USE_TZ = True


#########################################
# Static files (CSS, JavaScript, Images)#
#########################################
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = "/static/"
STATIC_ROOT = os.path.join(APP_ROOT_PATH, "static")


########################
# Application settings #
########################

# # login related
# LOGIN_REDIRECT_URL = '/'
# LOGIN_URL = '/accounts/login/'
# LOGOUT_REDIRECT_URL = '/'

# # HTTPの起点を指定
# ROOT_URLCONF = 'application.views.urls'

# if DEBUG:
#     # 3rd party development apps
#     INSTALLED_APPS.append("django_extensions")
#     INSTALLED_APPS.append("debug_toolbar")
#     MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")


###########
# Testing #
###########


###########
# Logging #
###########
