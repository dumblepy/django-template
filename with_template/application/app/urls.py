"""
例

sample_urls = [
    path('', SmapleViews.index),
    path('create/', SmapleViews.create),
    path('store/', SmapleViews.store),
    path('<int:id>/', SmapleViews.show),
    path('<int:id>/edit/', SmapleViews.edit),
    path('<int:id>/update/', SmapleViews.update),
    path('<int:id>/destroy/', SmapleViews.destroy),
]

app_urls = [
    path('samples/', include(sample_urls)),
]
"""

from django.urls import path, include

app_urls = [

]