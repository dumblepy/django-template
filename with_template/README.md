# django-docker-tempalte

Django + PostgreSQL + Nginx + gunicorn設定済みの雛形


### 本番環境で使う場合

※ 参 照
./docker/django/start-server.sh

```bash
# 環境変数ファイル作成
$ touch .env
# gunicorn + NginxでDjangoを動かすための設定
$ echo "DJANGO_ENV=production" >> .env
```

### 環境変数
以下の環境変数を.envに記入

|変数名|説明|
|--|--|
|DJANGO_ENV|local/dev/production localではdocker無しで起動可能|
|ALLOWED_HOSTS|サーバのホスト名またはアドレス|
|SECRET_KEY|Djangoのsettings.pyで記述する秘密鍵|
|DB_NAME|DBの名前|
|DB_USER|DBにログインするユーザ|
|DB_PASSWORD|DBにログインするユーザのパスワード|
|TZ|タイムゾーン|
|REDIS_PASSWORD|Redisのパスワード|


```bash
# 以下は例なので案件毎に適宜変更して下さい

$ echo "DJANGO_ENV=dev" >> .env
$ echo 'ALLOWED_HOSTS=127.0.0.1, localhost, 172.31.44.55' >> .env
$ echo 'SECRET_KEY=x!#3nwjted_=u5hq!e&yv#eq8tx+jh0a^vl2rzc)$78v+(hgf9' >> .env
$ echo 'DB_NAME=sample' >> .env
$ echo 'DB_USER=docker' >> .env
$ echo 'DB_PASSWORD=docker' >> .env
$ echo 'TZ=Asia/Tokyo' >> .env
$ echo 'REDIS_PASSWORD=2a0b932f602e40caa4cda9b9723f91ff' >> .env
```

#### 環境変数を変更したら
djangoコンテナのコンテナIDを確認
```
docker ps
>> c19cb6e76649
```
コンテナをビルドし直して再起動
```
docker-compose stop && docker rm c19cb6e76649 && docker-compose build && docker-compose up -d
```


dockerの環境変数(.env)ファイル
- https://docs.docker.com/compose/environment-variables/#the-env-file

- http://docs.docker.jp/compose/env-file.html

### 初回起動手順
```bash
# dockerイメージのビルド
$ make image # docker-compose -f docker-compose.yml build

# コンテナの起動
$ make up # docker-compose -f docker-compose.yml up -d
```

```bash
# マイグレーション
$ make migrate # docker-compose run --rm django python3 manage.py migrate
```


<img width="1677" alt="startproject" src="https://user-images.githubusercontent.com/46434778/61697152-aa34af80-ad71-11e9-92aa-d4c98e3e4c00.png">



## Pythonライブラリの管理方法
ライブラリ同士の依存関係を管理できるよう、poetry経由でライブラリの追加や削除を行ってください。

https://github.com/sdispater/poetry

#### 追加または削除方法

1. Pythonコンテナに入ります。
```bash
docker-compose exec django bash
```

2. 下記コマンドでライブラリの追加
```bash
poetry add library_name
```

開発のみに利用するライブラリは下記コマンドで追加してください
```bash
poetry add library_name --dev
```


3. ライブラリを削除するときは下記コマンドで削除してください。
```bash
poetry remove library_name
```


### 開発中にPythonコンテナでライブラリを利用した処理を実行したい

1. Pythonコンテナに入ります。
```bash
docker-compose exec django bash
```

2. poetryが作成する仮想環境に入ります。
```bash
poetry shell
```

### 案件を始める時には・・・

デフォルトのpoetry.lockおよびpyproject.tomlファイルは2019年7月時点で最新の各ライブラリのバージョンおよび依存関係となっています。

その時の最新のものとするためにまずこちらのコマンドを実行してアップデートしてください。

1. Pythonコンテナに入ります。
```bash
docker-compose exec django bash
```

2. 下記のコマンドを実行します。

```bash
poetry update
```

## redisを使うときは

1. 下記のコメントアウトを外してコンテナを立ち上げて下さい。

```
docker-compose.yaml
・djangoコンテナのdepends_onのredis


・redisコンテナの設定

redis:
image: redis:latest
ports:
    - 6379:6379
env_file: .env
environment:
    REDIS_PASSWORD: ${REDIS_PASSWORD}
command: redis-server --appendonly yes --requirepass ${REDIS_PASSWORD}
volumes:
    - ./docker/redis:/data
```

```
application/config/settings/base.py

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "PASSWORD": env("REDIS_PASSWORD")
        }
    }
}
```


### redisコンテナから操作する

`docker-compose exec redis bash`

`redis-cli -a [password]`


keyとvalueをセットする: `set key value`

keyにひもづくvalueを取得する: `get key`

keyとvalueを削除する: `del key`


### djangoからredisを操作する

`docker-compose exec django bash`

`python manage.py shell`

```python
from django.core.cache import cache

# keyとvalueをセットする
cache.set('key', 'value')

# keyにひもづくvalueを取得する
cache.get('key')

# keyとvalueを削除する
cache.delete('key')
```

DjangoからRedisを操作するためのライブラリ「django-redis」のドキュメント↓

http://niwinz.github.io/django-redis/latest/

## RESTの概念
### Formから送る時
|動詞|URI|アクション|内容|
|---|---|---|---|
|GET|/posts|index|全件表示|
|GET|/posts/create|create|新規作成ページ|
|POST|/posts/confirm|create_confirm|新規作成確認ページ|
|POST|/posts/store|store|新規作成|
|GET|/posts/{id}|show|一件表示|
|GET|/posts/{id}/edit|edit|一件編集ページ|
|POST|/posts/{id}/confirm|edit_confirm|一件編集確認ページ|
|POST|/posts/{id}/update|update|一件編集|
|POST|/posts/{id}/destroy/confirm|destroy_confirm|一件削除確認ページ|
|POST|/posts/{id}/destroy|destroy|一件削除|

### APIサーバーの時
|動詞|URI|アクション|内容|
|---|---|---|---|
|GET|/posts|index|全件表示|
|POST|/posts/confirm|create_confirm|新規作成確認ページ|
|POST|/posts/store|store|新規作成|
|GET|/posts/{id}|show|一件表示|
|PATCH|/posts/{id}/confirm|edit_confirm|一件編集確認ページ|
|PATCH|/posts/{id}/update|update|一件編集|
|DELETE|/posts/{id}/destroy/confirm|destroy_confirm|一件削除確認ページ|
|DELETE|/posts/{id}/destroy|destroy|一件削除|

## プロジェクトオリジナルコマンド
### make_seed
開発用データの入ったJSONファイルを作成するコマンドです

- application/app/management/commands/make_seed.pyを編集する
- poetryの仮想空間を有効にする `poetry shell`
- `python manage.py make_seed`コマンドを実行
  - `app/fixtures/seed.json`が出力される
- `python manage.py loaddata app/fixtures/seed.json`コマンドを実行
  - DBにseed.jsonの内容が書き込まれる

### domain
DDDに沿ったファイルを自動作成するコマンドです  
引数: Domain(業務名をキャメルケースで)

`python manage.py domain ManageUsers`

- `app/views/manage_users_views.py`
- `app/domain/services/manage_users_service.py`
- `app/domain/domain_models/entities/manage_users_entity.py`
- `app/repositories/manage_users_repository.py`

が作成されます

### orator
Oratorのクラスを自動作成するコマンドです  
引数: Model(モデル名を頭文字大文字の英単語単数形で)

`python manage.py create_domain User`

- `app/orator/User.py`

が作成されます

## ディレクトリ構成について
```
.
├── __init__.py
├── admin.py
├── apps.py
├── domain
│   ├── __init__.py
│   ├── domain_models
│   │   ├── __init__.py
│   │   ├── entities
│   │   │   └── __init__.py
│   │   └── value_object.py
│   └── service
│       ├── __init__.py
│       └── domain_services
│             └── __init__.py
├── fixtures
│   └── seed.json
├── management
│   ├── __init__.py
│   └── commands
│       ├── __init__.py
│       ├── domain.py
│       ├── make_seed.py
│       └── orator.py
├── migrations
│   └── __init__.py
├── models.py
├── orator
│   └── __init__.py
├── repositories
│   └── __init__.py
├── tests.py
├── urls.py
└── views
    └── __init__.py
```





## DDD的アーキテクチャ
ユーザーへのCRUDを例にして説明する

- 何のためにやるか？
  - DBの1テーブル単位のCRUDという作り方から脱却し、業務用件から設計できるようにするため
  - 責任と関心を分離して、要件の変更時に変更箇所を少なくするため

実際のソースコードが以下のようになることを目指す
- バリューオブジェクトのメソッドはエンティティしか知らない
- エンティティのメソッドはサービスクラスしか知らない
- リポジトリのメソッドはサービスクラスしか知らない
- サービスクラスのメソッドはビューしか知らない

#### ルーティング
app/urls.py

- URLとビューのメソッドを紐付ける
```
from app.views.user_views import UserViews

users_urls = [
    path('', UsersViews.index),
    path('create/', UsersViews.create),
    path('store/', UsersViews.store),
    path('<int:id>/', UsersViews.show),
    path('<int:id>/edit/', UsersViews.edit),
    path('<int:id>/update/', UsersViews.update),
    path('<int:id>/destroy/', UsersViews.destroy),
]

app_urls = [
    path('users/', include(users_urls)),
]
```

### ビュー
app/views/user_views.py

- リクエストパラメータの取得
- サービスクラスのメソッドを実行
- エラーハンドリング
- レスポンスの返却

<details><summary>サンプルコード</summary><div>

```
from django.shortcuts import redirect, render
from rest_framework.decorators import api_view

from ..domain.services.users_service import UserService


class UserViews:
    @api_view(['GET'])
    def index(request):
        users = UserService.index()
        return render(request, 'users/index.html', {'users': users})

    @api_view(['GET'])
    def create(request):
        return render(request, 'users/create.html')

    @api_view(['POST'])
    def store(request):
        user = {
            'name': request.POST['name'],
            'email': request.POST['email'],
            'password': request.POST['password'],
            'permission': request.POST['permission']
        }
        try:
            UserService.store(**user)
        except Exception as e:
            return render(
                request,
                'users/create.html',
                {'user': user, 'error': str(e)}
            )

        return redirect('/users/')

    @api_view(['GET'])
    def show(request, id: int):
        user = UserService.show(id)
        return render(request, 'users/show.html', {'user': user})

    @api_view(['GET'])
    def edit(request, id: int):
        user = UserService.edit(id)
        return render(request, 'users/edit.html', {'user': user})

    @api_view(['POST'])
    def update(request, id: int):
        user = {
            'id': id,
            'name': request.POST['name'],
            'email': request.POST['email'],
            'permission': request.POST['permission']
        }
        try:
            UserService.update(**user)
        except Exception as e:
            return render(
                request,
                'users/edit.html',
                {'user': user, 'error': str(e)}
            )

        return redirect(f'/users/{id}/')

    @api_view(['POST'])
    def destroy(request, id: int):
        result = UserService.delete(id)
        if result:
            return redirect('/users/')

```
</div></details>

### サービスクラス
app/domain/services/users_service.py

- 参照系処理(SELECT)の場合
  - リポジトリのメソッドを実行して返り値(DBから取得した値)を取得
  - 引数にリポジトリからの返り値を入れてエンティティのインスタンスを生成
  - エンティティのメソッドを実行して返り値を取得
  - ビューにエンティティからの返り値を返す
- 登録/更新系処理(INSERT, UPDATE)の場合
  - 重複チェック→不正なら例外を発生
  - 引数にビューから渡された値を入れてエンティティのインスタンスを生成
  - エンティティのメソッドを実行して返り値を取得
  - ビューにエンティティからの返り値を返す
- 削除系処理(DELETE)の場合
  - リポジトリのメソッドを実行

<details><summary>サンプルコード</summary><div>

```
from ..domain_models.entities.user_entity import UserEntity
from ...repositories.users_repository import UserRepository


class UserService:
    @staticmethod
    def index():
        users = UserRepository.index()
        users = [
            UserEntity(
                id=val['id'],
                name=val['name'],
                email=val['email'],
                permission=val['permission']
            ).get_index_dict()
            for val in users
        ]
        return users

    @staticmethod
    def store(name: str=None, email: str=None, password: str=None,
                permission: str=None):
        """store new user.

        Args:
            name (str): user name
            email (str): user email
            password (str): user password
            permission (str): user permission
        """
        # 重複チェック
        duplicate_users = UserRepository.check_unique(name, email)
        if duplicate_users:
            print(duplicate_users)
            raise Exception('name and email should be unique')

        user = UserEntity(
            name=name,
            email=email,
            password=password,
            permission=permission
        ).get_store_dict()
        result = UserRepository.store(user)
        return result

    @staticmethod
    def show(id: int):
        user = UserRepository.show(id)
        user = UserEntity(
            id=user['id'],
            name=user['name'],
            email=user['email'],
            permission=user['permission_id'],
            created_at=user['created_at'],
            updated_at=user['updated_at']
        ).get_show_dict()
        return user

    @staticmethod
    def edit(id: int):
        user = UserRepository.show(id)
        user = UserEntity(
            id=user['id'],
            name=user['name'],
            email=user['email'],
            permission=user['permission_id'],
            created_at=user['created_at'],
            updated_at=user['updated_at']
        ).get_edit_dict()
        return user

    def update(id: int, name: str, email: str, permission: str):
        """update user.

        Args:
            id (int): primary_ley
            name (str): user name
            email (str): user email
            permission (str): user permission
        """
        # 重複チェック
        duplicate_users = UserRepository.check_unique(name, email, id=id)
        if duplicate_users:
            print(duplicate_users)
            raise Exception('name and email should be unique')

        user = UserEntity(
            name=name,
            email=email,
            permission=permission
        ).get_update_dict()
        new_user = UserRepository.update(id, user)
        return new_user

    @staticmethod
    def delete(id: int):
        """delete user.

        Args:
            id (int): user id

        Returns:
            bool: if delete succress, reutrn True, or return False.
        """
        result = UserRepository.delete(id)
        return result

```
</div></details>

### エンティティ
app/domain/domain_models/entities/user_entity.py

- 一つの業務で扱うテーブルの、1レコード(行)の定義を持つクラス
  - 可変
  - 区別される：例えば同姓同名かもしれないので識別子が必要
  - 同一性を持つ：名称が変わっても同一人物なので識別子が必要
  - 値と振る舞い(パラメータとメソッド)を持つ
- 振る舞いがないパラメータにはプリミティブな値、振る舞いを持つパラメータはバリューオブジェクトを持つ
- インスタンス生成時に、引数で渡された値を元にパラメータをセットする
- サービスクラスで実行される、複数のパラメータをまとめてdictで返却するためのメソッドを持つ

<details><summary>サンプルコード</summary><div>

```
from ..value_objects import (
    EmailValueObject,
    PermissionValueObject,
    PasswordValueObject,
    DateTimeValueObject
)

class UserEntity:
    def __init__(self, id:int=None, name:str=None, email:str=None,
                password:str=None, permission_id:int=None, permission=None,
                created_at=None, updated_at=None):
        self.id = id
        self.name = name
        self.email = EmailValueObject(email)
        self.password = PasswordValueObject(password)
        self.permission = PermissionValueObject(permission)
        self.created_at = DateTimeValueObject(created_at)
        self.updated_at = DateTimeValueObject(updated_at)

    def get_index_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email.get_label(),
            'permission': self.permission.get_label()
        }

    def get_store_dict(self):
        return {
            'name': self.name,
            'email': self.email.get_label(),
            'password': self.password.get_hashed_value(),
            'permission_id': self.permission.get_number(),
            'created_at': self.created_at.get_label(),
            'updated_at': self.updated_at.get_label()
        }

    def get_show_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email.get_label(),
            'permission': self.permission.get_label(),
            'created_at': self.created_at.get_label(),
            'updated_at': self.updated_at.get_label()
        }

    def get_edit_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'email': self.email.get_label(),
            'permission': self.permission.get_label(),
        }

    def get_update_dict(self):
        return {
            'name': self.name,
            'email': self.email.get_label(),
            'permission_id': self.permission.get_number(),
            'updated_at': self.updated_at.get_label()
        }

```
</div></details>

### バリューオブジェクト
app/domain/domain_models/value_objects.py

- エンティティの一つのパラメータの値と振る舞いを表現するクラス
- 不変
- プライベートなパラメータを一つだけ持つ
- インスタンス生成時に値のバリデーションチェックをする→正常ならパラメータに値をセット、不正なら例外を発生する
- パラメータを返却するメソッドを持つ

<details><summary>サンプルコード</summary><div>

```
from django.contrib.auth.hashers import make_password
import re

class EmailValueObject:
    def __init__(self, value):
        EMAIL_REGEX = re.compile(
            r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$")
        if not EMAIL_REGEX.match(value):
            raise Exception(f'invalid email {value}')
        else:
            self.__value = value

    def get_label(self):
        return self.__value

class PasswordValueObject:
    def __init__(self, value):
        self.__value = value

    def get_hashed_value(self):
        return make_password(self.__value)
```
</div></details>

### リポジトリ
app/repositories/users_repository.py

- データの永続化処理を行うクラス
- 一般的にはRDBを使うが、NoSQLやファイル出力の場合もある
- RDBを使う場合には、ORMを実行する
- DBから取得したデータはlist/dictで返す

<details><summary>サンプルコード</summary><div>

```
from app.orator.User import User


class UserRepository:
    @staticmethod
    def index():
        return User \
            .select(
                'users.id as id',
                'users.name as name',
                'users.email as email',
                'permissions.permission as permission',
            ) \
            .join('permissions', 'users.permission_id', '=', 'permissions.id') \
            .order_by('users.id') \
            .get() \
            .serialize()

    @staticmethod
    def show(id: int):
        user = User.find(id).serialize()
        return user

    @staticmethod
    def store(user):
        return User.insert(user)

    @staticmethod
    def update(id, user):
        return User.where('id', id).update(user)

    @staticmethod
    def delete(id: int):
        return User.find(id).delete()

    @staticmethod
    def check_unique(name:str, email:str, id:int=None):
        if id:
            return User \
                .where('id', '!=', id) \
                .where(
                    User.where('name', name).or_where('email', email)
                ) \
                .get() \
                .serialize()
        else:
            return User \
                .where('name', name) \
                .or_where('email', email) \
                .get() \
                .serialize()
```
</div></details>

