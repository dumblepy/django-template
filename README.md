Django-template
===

## Django標準テンプレートエンジンを使ったプロジェクトをcloneする方法
```
git clone https://github.com/GOOYA/django-docker-template.git PROJECT_NAME
cd PROJECT_NAME
git filter-branch --subdirectory-filter with_template HEAD
```

## VueJSを使ったプロジェクトをcloneする方法
```
git clone https://github.com/GOOYA/django-docker-template.git PROJECT_NAME
cd PROJECT_NAME
git filter-branch --subdirectory-filter with_vue HEAD
```

## ReactJSを使ったプロジェクトをcloneする方法
```
git clone https://github.com/GOOYA/django-docker-template.git PROJECT_NAME
cd PROJECT_NAME
git filter-branch --subdirectory-filter with_react HEAD
```